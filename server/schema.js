const { merge } = require("lodash");
const { gql } = require("apollo-server");

const rootSchema = [
  gql`
    type Todo {
      todo_id: String!
      completed: Boolean!
      description: String
    }

    type Query {
      todos: [Todo]
    }

  `
];

// return [
//   { todo_id: 1, completed: false, description: "hello" },
//   { todo_id: 2, completed: true, description: "world" }
// ];

const rootResolvers = {
  Query: {
    todos(parent, args, context, info) {
      console.log("root", parent);
      console.log("args", args);
      console.log("context", context);
      console.log("info", info);

      return context.Todo.query().select();
    }
  }
};

// Put schema together into one array of schema strings
// and one map of resolvers, like makeExecutableSchema expects
const typeDefs = [...rootSchema];
const resolvers = merge(rootResolvers);

module.exports = {
  typeDefs,
  resolvers
};

// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
// const typeDefs = gql`
//   # Comments in GraphQL are defined with the hash (#) symbol.
//
//   # This "Book" type can be used in other type declarations.
//   type Book {
//     title: String
//     author: String
//   }
//
//   # The "Query" type is the root of all GraphQL queries.
//   # (A "Mutation" type will be covered later on.)
//   type Query {
//     books: [Book]
//   }
//
// `;

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
// const resolvers = {
//   Query: {
//     books: () => books
//   }
// };
