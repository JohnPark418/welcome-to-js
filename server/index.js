const { ApolloServer, gql } = require("apollo-server");
const { registerServer } = require("apollo-server-express");
const expressHandlebars = require("express-handlebars");
const express = require("express");
const Handlebars = require("handlebars");

const { typeDefs, resolvers } = require("./schema");

// const routes = require("routes");
const adminApp = require("./routes/admin");

const { Model } = require("objection");
const Todo = require("./models/Todo");

// Initialize knex.
const knex = require("knex")({
  client: "postgres",
  useNullAsDefault: true,
  connection: {
    host: "127.0.0.1",
    user: "logicsquare",
    password: "logicsquare",
    database: "fund",
    port: 55432
  }
});

// Give the knex object to objection.
Model.knex(knex);

const app = express();

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: { Todo: Todo }
});

app.engine("handlebars", expressHandlebars({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

app.get("/", (req, res) => res.send("hello world!"));
app.use("/admin", adminApp);

app.listen(4444, () => console.log("Example app listening on port 3000!"));

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
// server.listen().then(({ url }) => {
//   console.log(`🚀  Server ready at ${url}`);
// });

registerServer({ server, app });
