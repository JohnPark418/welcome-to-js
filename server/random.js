// const Sequelize = require("sequelize");
// 'postgres://logicsquare:logicsquare@localhost:55432/fund'

// const sequelize = new Sequelize({
//   database: "fund",
//   username: "logicsquare",
//   password: "logicsquare",

//   port: 55432,
//   dialect: "postgres"
// });
//
// const Todo = sequelize.define('todo', {
//   todo_id: Sequelize.BIGINT,
//   completed: Sequelize.BOOLEAN,
//   description: Sequelize.TEXT
// });
//
// Todo.findAll({ attributes: ['completed', 'todo_id', 'description']}).then((todos) => {
//   console.log("todos ", todos);
//
// });
//
// sequelize
//   .authenticate()
//   .then(() => {
//     console.log("Connection has been established successfully.");
//   })
//   .catch(err => {
//     console.error("Unable to connect to the database:", err);
//   });

// This is a (sample) collection of books we'll be able to query
// the GraphQL server for.  A more complete example might fetch
// from an existing data source like a REST API or database.
const books = [
  {
    title: "Harry Potter and the Chamber of Secrets",
    author: "J.K. Rowling"
  },
  {
    title: "Jurassic Park",
    author: "Michael Crichton"
  }
];
