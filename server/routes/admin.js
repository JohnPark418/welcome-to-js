const path = require("path");
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  console.log("Admin / requested");

  console.log("__dirname ", __dirname);
  res.sendFile(path.join(__dirname, "../public/index.html"));
});

module.exports = router;
