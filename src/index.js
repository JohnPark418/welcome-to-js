import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

import { ApolloProvider, Query } from "react-apollo";
import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

let uri = "https://w5xlvm3vzz.lp.gql.zone/graphql";
let uri1 = "http://localhost:4444/graphql";

const ExchangeRates = () => (
  <Query
    query={gql`
      {
        todos {
          completed
          todo_id
          description
        }
      }
    `}
  >
    {res => {
      let { loading, error, data } = res;
      console.log("res", res);
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return data.todos.map(todo => {
        let { completed, todo_id, description } = todo;
        console.log("Todo => ", todo);
        return (
          <div key={todo_id}>
            <p>{completed}</p>
            <p>{description}</p>
          </div>
        );
      });
    }}
  </Query>
);

const client = new ApolloClient({ uri: uri1 });

// client
// .query({
//   query: gql`
//     {
//       rates(currency: "USD") {
//         currency
//       }
//     }
//   `
// }).then(result => console.log(result));

const ApolloApp = () => (
  <ApolloProvider client={client}>
    <ExchangeRates />
  </ApolloProvider>
);

ReactDOM.render(<ApolloApp />, document.getElementById("root"));
registerServiceWorker();
